import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';

const Quran = () => {
  return (
    <ImageBackground
      source={require('../assets/image/bg.png')}
      style={{flex: 1, height: '100%'}}>
      <View style={[styles.container, {backgroundColor: 'rgba(0, 0, 0, 0.5)'}]}>
        <View>
          <View style={styles.tableRow}>
            <Text style={{fontSize: 18, color: '#fff', padding: 10}}>
              Quran
            </Text>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 4,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  table: {
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 5,
    marginBottom: 10,
  },
  tableRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    borderTopWidth: 1,
    borderTopColor: '#ddd',
    borderLeftWidth: 1,
    borderLeftColor: '#ddd',
    borderRightWidth: 1,
    borderRightColor: '#ddd',
    margin: 5,
    backgroundColor: '#808080',
  },

  headerRow: {
    backgroundColor: '#f5f5f5',
  },
  tableCell: {
    flex: 1,
    padding: 10,
    textAlign: 'center',
  },
  tableCell2: {
    flex: 1,
    padding: 10,
    textAlign: 'center',
    width: '30%',
  },
  headerCell: {
    fontWeight: 'bold',
  },
  prayerTitle: {
    fontWeight: 'bold',
    textAlign: 'left',
    paddingLeft: 10,
    fontSize: 18,
    color: '#fff',
  },
  prayerSalat: {
    fontWeight: 'bold',
    fontSize: 18,
    // textAlign: 'left',
    // paddingLeft: 10,
    color: '#fff',
  },
  prayerTitle2: {
    fontWeight: 'bold',
    textAlign: 'left',
    paddingLeft: 10,
  },
});

export default Quran;
