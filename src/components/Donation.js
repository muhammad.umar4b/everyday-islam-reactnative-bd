import React from 'react';
import {Image, ImageBackground, StyleSheet, Text, View} from 'react-native';

const Donation = () => {
  const donationCode = '123 7890 7860';

  return (
    <ImageBackground
      source={require('../assets/image/bg.png')}
      style={{flex: 1, height: '100%'}}>
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={require('../assets/image/QRcode.png')}
        />
        <Text style={styles.code}>{donationCode}</Text>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
  },
  code: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 20,
  },
});
export default Donation;
