import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  categoryArea: {
    marginHorizontal: wp('3%'),
    marginTop: hp('1%'),
  },
  categoryHeaderText: {
    marginHorizontal: wp('2%'),
    marginBottom: hp('0.5%'),
    fontWeight: '700',
  },
  categoryAreaSingle: {
    borderRadius: 8,
    marginBottom: hp('2%'),
    marginHorizontal: wp('2%'),
  },
  categoryView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp('90%'),
    paddingHorizontal: wp('3%'),
    paddingVertical: hp('3%'),
    borderRadius: 8,
  },
  categoryViewActive: {
    backgroundColor: '#D2181B',
    borderBottomStartRadius: 0,
    borderBottomEndRadius: 0,
  },
  foodItemArea: {
    paddingHorizontal: hp('2%'),
    borderWidth: 1,
    borderColor: '#D2181B',
    borderTopWidth: 0,
    borderBottomStartRadius: 8,
    borderBottomEndRadius: 8,
  },
  dataHeaderText: {
    fontSize: 20,
    fontWeight: '700',
    paddingBottom: hp('1.5%'),
  },
  detailsArea: {
    paddingVertical: hp('5%'),
    paddingHorizontal: hp('2%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopColor: '#dcd6d6',
    borderTopWidth: 1,
  },
  detailsAreaLeft: {
    width: wp('70%'),
  },
  detailsAreaTitle: {
    fontSize: 18,
    color: '#2e3333',
    fontWeight: '700',
  },
  detailsAreaDescription: {
    fontSize: 14,
    color: '#828585',
    paddingTop: hp('0.5%'),
  },
  detailsAreaAmount: {
    fontSize: 14,
    color: '#828585',
    paddingTop: hp('0.5%'),
  },
  modalHeaderArea: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
    paddingHorizontal: wp('4%'),
    paddingVertical: hp('2%'),
    borderBottomWidth: 1,
    borderBottomColor: '#CDC9C9',
  },
  modalHeaderField: {
    fontSize: wp('5%'),
    color: '#000',
    fontWeight: '600',
  },
  modalCloseIconArea: {
    backgroundColor: '#D2181B',
    width: 30,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  modalBody: {
    backgroundColor: '#fff',
    paddingHorizontal: wp('4%'),
    paddingBottom: hp('3.5%'),
  },
  modalContentField: {
    textAlign: 'justify',
    lineHeight: 23,
    fontSize: 16,
    color: '#000000',
    paddingTop: hp('1%'),
  },
  addToArea: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: hp('4%'),
    width: wp('50%'),
  },
  addToButtonArea: {
    marginTop: hp('5%'),
    borderRadius: 3,
    backgroundColor: '#D2181B',
    paddingVertical: hp('2%'),
  },
  addToButton: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  viewBasketArea: {
    // width: wp('100%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: hp('4%'),
    paddingHorizontal: wp('4%'),
    elevation: 5,
    backgroundColor: '#D2181B',
  },
  viewBasketText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '700',
  },
  // restaurantInfoArea: {
  //   paddingHorizontal: wp('5%'),
  //   paddingTop: hp('1%'),
  // },
  // serviceTypeArea: {
  //   paddingTop: hp('0.5%'),
  //   paddingBottom: hp('1%'),
  // },
  // serviceArea: {
  //   paddingTop: hp('0.5%'),
  //   paddingBottom: hp('1%'),
  //   borderBottomColor: '#b4b4b4',
  //   borderBottomWidth: 1,
  //   margin: 50,
  // },
  // serviceAreaSingle: {
  //   paddingHorizontal: wp('4%'),
  //   paddingVertical: hp('1%'),
  //   flexDirection: 'row',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
  // activeService: {
  //   backgroundColor: '#D2181B',
  //   borderRadius: 20,
  // },
  menuBarServiceArea: {
    paddingHorizontal: wp('4%'),
    paddingVertical: hp('0.5%'),
    borderRadius: 15,
    backgroundColor: '#D2181B',
    marginRight: wp('5%'),
  },
  addonArea: {
    paddingTop: hp('3%'),
    paddingBottom: hp('1%'),
    borderBottomColor: '#CDC9C9',
    borderBottomWidth: 1,
    marginBottom: hp('0.5%'),
  },
  addonCheckBox: {
    borderColor: 'transparent',
    paddingVertical: hp('0%'),
    paddingHorizontal: wp('0%'),
    backgroundColor: '#fff',
  },
  specialOfferBlockArea: {
    marginTop: hp('2%'),
    paddingVertical: hp('1%'),
    borderRadius: 8,
    borderColor: '#eee',
    borderWidth: 1,
  },
  specialOfferBlockAreaHeader: {
    borderBottomColor: '#eee',
    borderBottomWidth: 1,
    paddingBottom: hp('1%'),
    paddingHorizontal: wp('2%'),
  },
  specialOfferBlockAreaBody: {
    paddingHorizontal: wp('2%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  specialOfferBlockAreaBodyLabel: {
    paddingTop: hp('1%'),
    fontSize: 16,
  },

  restaurantInfoArea: {
    // paddingHorizontal: wp('5%'),
    // paddingTop: hp('1%'),
    // width: '100%',
  },
  serviceTypeArea: {
    paddingTop: hp('0.5%'),
    paddingBottom: hp('1%'),
    // width: '100%',
  },
  serviceArea: {
    paddingTop: hp('0.7%'),
    paddingBottom: hp('1%'),
    // marginRight: 0,
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
    width: '200%',
  },
  serviceAreaSingle: {
    paddingHorizontal: wp('7%'),
    paddingVertical: hp('1%'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#000',
    // width: '35%',
  },
  activeService: {
    backgroundColor: '#D2181B',
    borderRadius: 20,
    color: '#fff',
  },

  menuBarServiceArea: {
    paddingHorizontal: wp('4%'),
    paddingVertical: hp('0.5%'),
    borderRadius: 15,
    backgroundColor: '#D2181B',
    marginRight: wp('5%'),
  },
});

export default styles;
