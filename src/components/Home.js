import axios from 'axios';
import React, {useContext, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
// import Geolocation from 'react-native-geolocation-service';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {GlobalContext} from '../context/GlobalContext';
import globalStyles from '../styles/globalStyles';

const Home = ({navigation}) => {
  const {globalState, setGlobalState} = useContext(GlobalContext);
  let [mosqueData, setMosqueData] = useState([]);
  let [isVisible, setIsVisible] = useState(false);
  const [cities, setCities] = useState('');
  const [inputValue, setInputValue] = useState('');
  const [suggestions, setSuggestions] = useState([]);
  const [isVisibleCities, setIsVisibleCities] = useState(true);

  const handleInputChange = value => {
    setInputValue(value);
    const filteredCities = BangladeshCities.filter(city =>
      city.toLowerCase().startsWith(value.toLowerCase()),
    );
    setCities(cityy);
    setSuggestions(filteredCities);
  };

  const BangladeshCities = [
    'Dhaka,Bangladesh',
    'Chittagong,Bangladesh',
    'Khulna,Bangladesh',
    'Rajshahi,Bangladesh',
    'Sylhet,Bangladesh',
    'Barisal,Bangladesh',
    'Rangpur,Bangladesh',
    'Narayanganj,Bangladesh',
    'Gazipur,Bangladesh',
    'Mymensingh,Bangladesh',
    'Comilla,Bangladesh',
    'Jessore,Bangladesh',
    'Bogra,Bangladesh',
    'Dinajpur,Bangladesh',
    'Nawabganj,Bangladesh',
  ];

  // const [longitude, setLongitude] = useState('');
  // const [latitude, setLatitude] = useState('');
  // // const key = 'AIzaSyA7ks8X2YnLcxTuEC3qydL2adzA0NYbl6c';

  // // const requestLOCATIONPermission = async () => {
  // //   try {
  // //     const granted = await PermissionsAndroid.request(
  // //       PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  // //       {
  // //         title: 'Cool Photo App LOCATION Permission',
  // //         message:
  // //           'Cool Photo App needs access to your LOCATION ' +
  // //           'so you can take awesome pictures.',
  // //         buttonNeutral: 'Ask Me Later',
  // //         buttonNegative: 'Cancel',
  // //         buttonPositive: 'OK',
  // //       },
  // //     );
  // //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  // //       Geolocation.getCurrentPosition(
  // //         position => {
  // //           setLatitude(position.coords.latitude);
  // //           setLongitude(position.coords.longitude);
  // //         },
  // //         error => {
  // //           console.log(error.code, error.message);
  // //         },
  // //         {
  // //           enableHighAccuracy: true,
  // //           timeout: 10000,
  // //           maximumAge: 10000,
  // //         },
  // //       );
  // //       console.log('You can use the LOCATION');
  // //     } else {
  // //       console.log('LOCATION permission denied');
  // //     }
  // //   } catch (err) {
  // //     console.warn(err);
  // //   }
  // // };

  // useEffect(() => {
  //   // axios
  //   //   .request({
  //   //     method: 'post',
  //   //     url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${key}`,
  //   //   })
  //   //   .then(response => {
  //   //     const firstResult = response?.data?.results[1];
  //   //     // console.log('first', response?.data?.results[1].address_components);
  //   //     // const postalCodeComponent2 = firstResult?.address_components.find(
  //   //     //   component => component?.types.includes('administrative_area_level_2'),
  //   //     // ).long_name;
  //   //     // const postalCodeComponent1 = firstResult?.address_components.find(
  //   //     //   component => component?.types.includes('administrative_area_level_1'),
  //   //     // ).long_name;
  //   //     const postalCodeComponent3 = firstResult?.address_components.find(
  //   //       component => component?.types.includes('administrative_area_level_3'),
  //   //     ).long_name;
  //   //     const country = firstResult?.address_components.find(component =>
  //   //       component?.types.includes('country'),
  //   //     ).long_name;
  //   //     setCities(`${postalCodeComponent3} , ${country}`);
  //   //   })
  //   //   .catch(e => {
  //   //     console.log(e.response);
  //   //   });
  //   // requestLOCATIONPermission();
  // }, [longitude, latitude]);

  const mosqueHandler = async () => {
    console.log('mosque', cities);
    setIsVisible(true);
    try {
      const response = await axios.get(
        `https://api.everydayislam.org/api/get/mosque-by-city/${cities}`,
      );
      setMosqueData(response.data);
      setIsVisible(false);
      return response.data;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const timingHandler = async mosqueId => {
    setIsVisible(true);
    try {
      if (mosqueId) {
        const responseTiming = await axios.get(
          `https://api.everydayislam.org/api/get/timings/${mosqueId}`,
        );
        setGlobalState({...globalState, timing: responseTiming.data});
        setIsVisible(false);
        return responseTiming.data;
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const handleSelectCity = city => {
    setIsVisibleCities(false);
    setCities(city);
    setInputValue(city);
  };
  return (
    <ImageBackground
      source={require('../assets/image/bg.png')}
      style={{flex: 1}}>
      <View
        style={[
          globalStyles.container,
          {backgroundColor: 'rgba(0, 0, 0, 0.5)'},
        ]}>
        <ScrollView showsHorizontalScrollIndicator={false}>
          <View style={[styles.searchArea, globalStyles.boxShadow]}>
            <View>
              <TextInput
                style={styles.searchInput}
                value={inputValue}
                onChangeText={handleInputChange}
                placeholder="Enter City Name"
                placeholderTextColor={'#fff'}
              />
              {isVisibleCities &&
                suggestions.map((suggestion, index) => (
                  <Text
                    key={index}
                    style={{
                      padding: 10,
                      borderBottomWidth: 1,
                      borderBottomColor: '#ccc',
                      color: '#fff',
                    }}
                    onPress={() => handleSelectCity(suggestion)}>
                    {suggestion}
                  </Text>
                ))}
            </View>
            <View>
              {/* <TextInput
              style={styles.searchInput}
              value={cities}
              onChangeText={value => {
                setCities(value);
              }}
              onFocus={() => {
                navigation.setOptions({
                  tabBarVisible: false,
                });
              }}
              onBlur={() => {
                navigation.setOptions({
                  tabBarVisible: true,
                });
              }}
              keyboardType={'default'}
              placeholderTextColor="white"
              placeholder="Enter City , United Kingdom"
            /> */}

              <TouchableOpacity onPress={() => mosqueHandler()}>
                <AntDesign
                  style={styles.searchIconLocation}
                  name="search1"
                  size={35}
                  color="White"
                />
              </TouchableOpacity>
            </View>
          </View>
          {isVisible ? (
            <ActivityIndicator size="small" color="#ffffff" />
          ) : (
            mosqueData.map(mosque => (
              <View key={mosque.id}>
                <TouchableOpacity onPress={() => timingHandler(mosque.id)}>
                  <View styles={{flexDirection: 'row'}}>
                    <Text
                      style={[
                        {color: '#fff'},
                        // {marginLeft: 4},
                        {padding: 15, backgroundColor: 'rgba(0, 0, 0, 0.5)'},
                        {height: 50, borderRadius: 100},
                      ]}>
                      {mosque.name}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            ))
          )}
          <View>
            <Text style={styles.logo}>EVERYDAY ISLAM</Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'center',
            }}>
            <View style={{width: 120}}>
              <TouchableOpacity
                onPress={() => navigation.navigate('PrayerTime')}>
                <View style={styles.menuItem}>
                  <Image
                    source={require('../assets/image/ic_duas.png')}
                    style={styles.menuItemImage}
                  />
                  <Text style={styles.menuItemText}>Prayer Time</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{width: 120}}>
              <TouchableOpacity onPress={() => navigation.navigate('AzanTime')}>
                <View style={styles.menuItem}>
                  <Image
                    source={require('../assets/image/ic_prayertime.png')}
                    style={styles.menuItemImage}
                  />
                  <Text style={styles.menuItemText}>Azan</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{width: 120}}>
              <TouchableOpacity onPress={() => navigation.navigate('Quran')}>
                <View style={styles.menuItem}>
                  <Image
                    source={require('../assets/image/ic_qurans.png')}
                    style={styles.menuItemImage}
                  />
                  <Text style={styles.menuItemText}>Quran</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{width: 120}}>
              <TouchableOpacity onPress={() => navigation.navigate('Hadith')}>
                <View style={styles.menuItem}>
                  <Image
                    source={require('../assets/image/ic_books.png')}
                    style={styles.menuItemImage}
                  />
                  <Text style={styles.menuItemText}>Hadith</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{width: 120}}>
              <TouchableOpacity onPress={() => navigation.navigate('Duaa')}>
                <View style={styles.menuItem}>
                  <Image
                    source={require('../assets/image/ic_amaals.png')}
                    style={styles.menuItemImage}
                  />
                  <Text style={styles.menuItemText}>Daily Dua</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{width: 120}}>
              <TouchableOpacity onPress={() => navigation.navigate('Donation')}>
                <View style={styles.menuItem}>
                  <Image
                    source={require('../assets/image/ic_donate.png')}
                    style={styles.menuItemImage}
                  />
                  <Text style={styles.menuItemText}>Donation</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  logo: {
    marginTop: 110,
    fontSize: 28,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#1fc420',
    // color: '#21c321',
  },
  searchArea: {
    flexDirection: 'row',
    borderRadius: 5,
    marginVertical: hp('1%'),
    marginHorizontal: wp('0.5%'),
  },
  searchInput: {
    paddingVertical: hp('2%'),
    paddingLeft: wp('4%'),
    width: wp('78%'),
    paddingTop: 14,
    color: '#fff',
  },
  searchIcon: {
    paddingVertical: hp('1.5%'),
    width: wp('11.5%'),
    textAlign: 'center',
    backgroundColor: '#D2181B',
    color: '#fff',
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  searchIconLocation: {
    // marginTop: 1,
    marginRight: 5,
    paddingVertical: hp('1.499%'),
    width: wp('11.5%'),
    textAlign: 'center',
    backgroundColor: '#21c321',
    // backgroundColor: '#575a5f',
    // borderColor: '#D2181B',
    // borderWidth: 1,
    color: '#fff',
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  menuItem: {
    textAlign: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    padding: 15,
    borderRadius: 6,
    flexDirection: 'column',
    alignItems: 'center',
    margin: '2%',
  },
  menuItemImage: {
    width: '50%',
    height: 60,
  },
  menuItemText: {
    fontSize: 13,
    marginVertical: 2,
    height: 20,
    overflow: 'hidden',
    color: '#fff',
  },
});

export default Home;
