import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React, {useContext} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Splash from '../components/auth/Splash';
import AzanTime from '../components/AzanTime';
import Donation from '../components/Donation';
import Duaa from '../components/Duaa';
import Hadith from '../components/Hadith';
/* Component */
import Home from '../components/Home';
import Lectures from '../components/Lectures';
import PrayerTime from '../components/PrayerTime';
import Quran from '../components/Quran';
// import Chat from "../components/Chat";
import Settings from '../components/Settings';
import {GlobalContext} from '../context/GlobalContext';

/* Bottom Tab Navigator */
const Tab = createBottomTabNavigator();
const HomeTabs = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#21c321',
        tabStyle: {
          backgroundColor: '#000',
          height: 50,
          marginTop: -1,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Settings"
        component={Settings}
        options={{
          tabBarIcon: ({color, size}) => (
            <AntDesign name="setting" size={size} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const withoutHeaderOptions = {
  headerShown: false,
  gestureEnabled: true,
  gestureDirection: 'horizontal',
};

const withHeaderOptions = {
  headerShown: true,
  gestureEnabled: true,
  gestureDirection: 'horizontal',
};

/* Stack Navigator */
const MainStack = createStackNavigator();
const MasterRoute = () => {
  const {globalState} = useContext(GlobalContext);

  return (
    <NavigationContainer>
      <MainStack.Navigator>
        <MainStack.Screen
          options={withoutHeaderOptions}
          name={'Splash'}
          component={Splash}
        />
        <MainStack.Screen
          options={withoutHeaderOptions}
          name={'Home'}
          children={HomeTabs}
        />
        <MainStack.Screen
          options={{...withHeaderOptions, title: 'Prayer Time'}}
          name={'PrayerTime'}
          component={PrayerTime}
        />
        <MainStack.Screen
          options={{...withHeaderOptions, title: 'Adhan Time'}}
          name={'AzanTime'}
          component={AzanTime}
        />
        <MainStack.Screen
          options={{...withHeaderOptions, title: 'Quran'}}
          name={'Quran'}
          component={Quran}
        />
        <MainStack.Screen
          options={{...withHeaderOptions, title: 'Hadith'}}
          name={'Hadith'}
          component={Hadith}
        />
        <MainStack.Screen
          options={{...withHeaderOptions, title: 'Duaa'}}
          name={'Duaa'}
          component={Duaa}
        />
        <MainStack.Screen
          options={{...withHeaderOptions, title: 'Lectures'}}
          name={'Lectures'}
          component={Lectures}
        />
        <MainStack.Screen
          options={{...withHeaderOptions, title: 'Donation'}}
          name={'Donation'}
          component={Donation}
        />
      </MainStack.Navigator>
    </NavigationContainer>
  );
};

export default MasterRoute;
